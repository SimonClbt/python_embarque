#!/usr/bin/env python3

import os, sys
from PySide2.QtCore import Property, QUrl, QObject, Slot, Signal, QSocketNotifier
from PySide2.QtQml import QQmlApplicationEngine
from PySide2.QtGui import QGuiApplication, QColor
import serial
    	
class Backend(QObject):

    buttonPressed = Signal()
    game = Signal()
    

    def __init__ (self, parent=None):
        QObject.__init__(self, parent)
        self.port = serial.Serial('/dev/ttyACM1', timeout=1)
        self.port_file = QSocketNotifier(self.port.fileno(), QSocketNotifier.Read)
        self.port_file.activated.connect(self.portReadyRead)
        

    @Slot()
    def portReadyRead(self):
        data = self.port.readline()
        if data.startswith(b'[') and data.strip().endswith(b']'):
            self.data = eval(data)
            self.game.emit()
            print("Saisissez la liste de couleurs")
    
    colors = []
    @Slot(QColor)
    def color(self, color):
        print("couleur: %s" % color)
        colors.append(color)

if __name__ == '__main__':
    app = QGuiApplication(sys.argv)
    engine = QQmlApplicationEngine()

    backend = Backend()
    engine.rootContext().setContextProperty("backend", backend)

    view_file = os.path.join(os.path.dirname(__file__), 'ui.qml')
    engine.load(QUrl.fromLocalFile(view_file))
    app.exec_()
