J'ai voulu réaliser un jeu qui demande à l'utilisateur de mémoriser une suite de couleur qui apparaît aléatoirement.
5 couleurs apparaissent sur la carte 5 fois
Ensuite via une interface graphique, l'utilisateur retranscrit la liste qu'il pense avoir vu
Après comparaison, il est notifié ou non de sa victoire.

Je n'ai pas réussi à gérer le renvoi des couleurs via l'interface graphique pour comparaison.

Dans l'absolu, j'aurais souhaité ajouter la notion de "niveau" avec apparation de plus de couleurs et de manière plus rapide par exemple.
