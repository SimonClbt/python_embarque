import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Window 2.12

ApplicationWindow {

	width: 800
	height: 600
	visible: true
	title: qsTr("SUPER SIMON")

	Text {
		anchors.horizontalCenter: parent.horizontalCenter 
    		text: "SUPER SIMON"
    		font.family: "Helvetica"
    		font.pointSize: 24
    		color: "black"
		}

	Row {
		anchors.centerIn: parent
		spacing: 10

		Button {
			width: 100
    			height: 100
    			id: blue
			background: Rectangle {
            			color: "#0000ff"
        		}
			text: "BLUE"
			onClicked: function() {
				backend.color(this, blue.background.color);
			}
			enabled: false
		}
		Button {
			width: 100
    			height: 100
    			id: yellow
			background: Rectangle {
            			color: "#c8c814"
        		}
			text: "YELLOW"
			onClicked: function() {
				backend.color(this, yellow.background.color);
			}
			enabled: false
		}
		Button {
			width: 100
    			height: 100
    			id: green
			background: Rectangle {
            			color: "#00ff00"
        		}
			text: "GREEN"
			onClicked: function() {
				backend.color(this, green.background.color);
			}
			enabled: false
		}
		Button {
			width: 100
    			height: 100
    			id: red
			background: Rectangle {
            			color: "#ff0000"
        		}
			text: "RED"
			onClicked: function() {
				backend.color(this, red.background.color);
			}
			enabled: false
		}
		Button {
			width: 100
    			height: 100
    			id: gray
			background: Rectangle {
            			color: "#c0c0c0"
        		}
			text: "GRAY"
			onClicked: function() {
				print('Gray pressed')
				backend.color(this, gray.background.color);
			}
			enabled: false
		}
	}
	Connections {
		target: backend
		
		
	}
}
