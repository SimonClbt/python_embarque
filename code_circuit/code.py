from adafruit_circuitplayground.express import cpx
import board
import time
import neopixel
import supervisor


try:
    import urandom as random
except ImportError:
    import random

def init_state():
    cpx.red_led = True
    time.sleep(0.5)
    cpx.red_led = False
    time.sleep(0.5)

while True:
    if not(supervisor.runtime.serial_connected):
        init_state()
        print("init")
        continue
    numpix = 10
    #pixpin = board.D8 
    #strip = neopixel.NeoPixel(pixpin, numpix, brightness=1, auto_write=True)
    strip = cpx.pixels
    colors = [
        [0, 0, 255],  # Blue
        [200, 200, 20],  # Yellow
        [0, 255, 0],  # Green 
        [255, 0, 0],  # Red
        [192, 192, 192],  # Gray
    ]
    myList = []     
    def flash_random(wait, howmany):
        for _ in range(howmany):
     
            c = random.randint(0, len(colors) - 1)  
            j = random.randint(0, numpix - 1)  
            strip[j] = colors[c]
            myList.append(colors[c])
     
        for i in range(1, 5):
            strip.brightness = 0.02  
            time.sleep(wait)
     
        for i in range(5, 0, -1):
            strip.brightness = 0.02  
            strip[j] = [0, 0, 0] 
            time.sleep(wait)
     
    turn = 0 
    while turn < 5:
        flash_random(0.2, 1)
        turn = turn + 1
    print(myList)
    while not(supervisor.runtime.serial_bytes_available):
    	time.sleep(0.5)
    
